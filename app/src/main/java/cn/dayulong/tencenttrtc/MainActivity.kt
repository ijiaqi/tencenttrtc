package cn.dayulong.tencenttrtc

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<TextView>(R.id.tv_start).setOnClickListener {
            joinRoom(it, false)
        }
    }

    override fun onResume() {
        super.onResume()
        CallService.start(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        CallService.stop(this)
    }
}