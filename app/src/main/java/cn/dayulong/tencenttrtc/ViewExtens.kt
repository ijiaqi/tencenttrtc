package cn.dayulong.tencenttrtc

import android.content.Context
import android.content.Intent
import android.view.View
import com.tencent.liteav.liveroom.ui.anchor.TCCameraAnchorActivity
import com.tencent.liteav.meeting.ui.CreateMeetingActivity
import com.tencent.liteav.meeting.ui.MeetingMainActivity
import com.tencent.trtc.TRTCCloudDef


/**
 * 进入直播间
 */
fun joinLiveOnlyAnchor(context: Context) {
    val intent = Intent(context, TCCameraAnchorActivity::class.java).apply {
//            putExtra("USER_ID", getUser().id)
//            putExtra("USER_NAME", roomBean.userName)
//            putExtra("ROOM_TITLE", roomBean.roomName)
//            putExtra("USER_AVATAR", getUrl(getUser().teacherHeadPictureUrl))
//            putExtra("COVER_PIC", getUrl(roomBean.coverPic.orEmpty()))
//            putExtra("ROOM_ID", roomBean.roomId)
    }
    context.startActivity(intent)
}

/**
 * 进入视频教室
 */
fun joinRoom(view: View, isOnlyAnchor: Boolean) {
    if (isOnlyAnchor) joinLiveOnlyAnchor(view.context)
    else joinLive(view.context)
//        joinMeeting(view.context, roomBean)
}

/**
 * 进入直播间
 */
fun joinLive(context: Context) {
    val intent = Intent(context, TCCameraAnchorActivity::class.java).apply {
//            putExtra(Parameters.USER_ID, getUser().id)
//            putExtra(Parameters.USER_NAME, roomBean.userName)
//            putExtra(Parameters.ROOM_TITLE, roomBean.roomName)
//            putExtra(Parameters.USER_AVATAR, getUrl(getUser().teacherHeadPictureUrl))
//            putExtra(Parameters.COVER_PIC, getUrl(roomBean.coverPic.orEmpty()))
//            putExtra(Parameters.ROOM_ID, roomBean.roomId)
    }
    context.startActivity(intent)
//        val userName =
//            if (getUser().teacherNickName.isNullOrEmpty()) getUser().nickName else getUser().teacherNickName
//        val intent = Intent(context, TCAudienceActivity::class.java).apply {
//            putExtra(TCConstants.ROOM_TITLE, roomBean.roomName)
//            putExtra(TCConstants.GROUP_ID, roomBean.roomId)
//            putExtra(TCConstants.USE_CDN_PLAY, true)
//            putExtra(TCConstants.PUSHER_ID, roomBean.userId)
//            putExtra(TCConstants.PUSHER_NAME, roomBean.userName)
//            putExtra(TCConstants.COVER_PIC, getUrl(roomBean.coverPic.orEmpty()))
//            putExtra(TCConstants.PUSHER_AVATAR, getUrl(roomBean.userAvatar.orEmpty()))
//            putExtra(Parameters.USER_ID, getUser().id)
//            putExtra(Parameters.USER_NAME, getUser().surname)
//            putExtra(
//                Parameters.USER_AVATAR,
//                getUrl(getUser().teacherHeadPictureUrl.orEmpty())
//            )
//        }
//        context.startActivity(intent)
}

/**
 * 进入会议室
 */
fun joinMeeting(context: Context) {
    //摄像头开启
    val mOpenCamera = true
    //麦克风开启
    val mOpenAudio = true
    //音质标准
    val mAudioQuality = TRTCCloudDef.TRTC_AUDIO_QUALITY_DEFAULT
    //画质流畅
    val mVideoQuality = CreateMeetingActivity.VIDEO_QUALITY_FAST
    MeetingMainActivity.enterRoom(
        context,
        123,
        "getUser().id",
        "getUser().teacherNickName",
        "getUrl(getUser().teacherHeadPictureUrl)",
        mOpenCamera,
        mOpenAudio,
        mAudioQuality,
        mVideoQuality
    )
}